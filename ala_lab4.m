%задание 1.1
A1 = [
    -2, 1;
    1, -2;
    ];

%задание 1.2
A2 = [
    1, -1;
    1, -1;
    ];

%задание 1.3
A3 = [
    0, 1;
    -4, 4;
    ];

%задание 1.4
A4= [
    -1, -1;
    1, -1;
    ];

%задание 1.5
A5= [
    1, -1;
    1, 1;
    ];

%задание 1.6
A6= [
    -1, -1;
    1, -1;
    ];

%%задание 3

%задание 3.1
K1 = [
    -5, 2;
    -8, 3;
    ];

%задание 3.2
K2 = [
    -sqrt(2)/2, sqrt(2)/2;
    -sqrt(2)/2, -sqrt(2)/2;
    ];

%задание 3.3
K3 = [
    -2, 5;
    -1, 2;
    ];


%задание 3.4
K4 = [
    -sqrt(2), -2.5*sqrt(2);
    sqrt(2), 2*sqrt(2);
    ];

%задание 3.5
K5 = [
    -5, 4;
    -9, 7;
    ];

%задание 3.6
K6 = [
    1.5, -4;
    1, -2.5;
    ];

%задание 3.7
K7 = [
    0, 1/2;
    -1/2, 1;
    ];

%задание 3.8
K8 = [
    2.5, -4;
    1, -1.5;
    ];

%задание 3.9
K9 = [
    0, -4;
    1, -4;
    ];

%задание 3.10
K10 = [
    -3, 4;
    -2, 4;
    ];

%задание 3.11
K11 = [
    4, -4;
    1, 0;
    ];

%задание 3.12
K12 = [
    2, -4;
    1, -2;
    ];

ic_1 = [1;1];
ic_2 = [1;4];
ic_3 = [-2;3];

ic_arr= [ic_1,ic_2,ic_3];
%{
A_arr = [A1; A2; A3; A4; A5; A6];
alen=6;
iclen=3;
for i=1:alen
   A=A_arr(2*(i-1)+1:2*i,:);
   for j=1:iclen
       ic=ic_arr(:,j,:);
       a = sim("ala_lab4",'StartTime','0','StopTime','5');
       X=a.get('X').Data;
       T=a.get("tout");
       figure(i);
       plot(X(:,2),X(:,1));
       hold on
       
       figure(i+alen);
       hold on
       plot(T(:,1),X(:,1));
       plot(T(:,1),X(:,2));
   end
   figure(i);
   grid on
   legend('ic_1','ic_2','ic_3');
   title("x_2(x_1)"+" matrix A"+num2str(i));
   saveas(gcf,"x_2(x_1)"+" matrix A"+num2str(i)+'.png')
   hold off
   
   figure(i+alen);
   grid on
   legend("x1_ic_1","x2_ic_1","x1_ic_2","x2_ic_2","x1_ic_3","x2_ic_3");
   title("x_1(t), x_2(t)"+" matrix A"+num2str(i));
   saveas(gcf,"x_1(t), x_2(t)"+" matrix A"+num2str(i)+'.png')
end
%}
%{
K_arr = [K1; K2; K3; K4; K5; K6; K7; K8; K9; K10; K11; K12];
klen=12;
iclen=3;
for i=1:klen
   K=K_arr(2*(i-1)+1:2*i,:);
   for j=1:iclen
       ic=ic_arr(:,j,:);
       a = sim("ala_lab4_dis",'StartTime','0','StopTime','1');

       X=a.get('X').Data;
       T=a.get("tout"); 

       figure(i);
       hold on
       stairs(T(:,1),X(:,1));
       stairs(T(:,1),X(:,2));
   end
   figure(i);
   grid on
   legend("x1_ic_1","x2_ic_1","x1_ic_2","x2_ic_2","x1_ic_3","x2_ic_3");
   title("x_1(t), x_2(t)"+" matrix K"+num2str(i));
   saveas(gcf,"x_1(t), x_2(t)"+" matrix K"+num2str(i)+'.png')
end
%}

a=1;
b=-1;

A = [
    0, 1;
    a, b;
    ];

figure(1)
hold on

ic=[1;1];
a = sim("ala_lab4",'StartTime','0','StopTime','10');

X=a.get('X').Data;
T=a.get("tout");

hold on
plot(T(:,1),X(:,1));
plot(T(:,1),X(:,2));

grid on
legend('x_1','x_2');
hold off